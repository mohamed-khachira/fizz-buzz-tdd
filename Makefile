.PHONY: test test-watch coverage

test: ## Run unit tests
	./vendor/bin/phpunit --testdox

test-watch: ## Automatically rerun PHPUnit tests when source code changes
	./vendor/bin/phpunit-watcher watch --testdox

coverage: ## Generate PHPUnit Coverage Report In HTML
	XDEBUG_MODE=coverage ./vendor/bin/phpunit --coverage-html coverage --coverage-filter src