<?php

class FizzBuzz
{
    public function run(int $minNumber, int $maxNumber): string
    {
        return $this->evaluateNumbers($minNumber, $maxNumber);
    }

    private function evaluateNumbers(int $minNumber, $maxNumber): string
    {
        $result = "";
        while ($minNumber <= $maxNumber)
            $result .= $this->evaluateNumber($minNumber++);
        return $result;
    }
    private function evaluateNumber(int $number): string
    {
        if ($number % 15 == 0)
            return "FizzBuzz";
        if ($number % 3 == 0)
            return "Fizz";
        if ($number % 5 == 0)
            return "Buzz";
        return $number;
    }
}
