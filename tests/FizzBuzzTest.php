<?php

require 'src/FizzBuzz.php';

use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    private FizzBuzz $fizzBuzz;

    protected function setUp(): void
    {
        $this->fizzBuzz = new FizzBuzz();
    }

    /**
     * @test
     */
    public function shouldReturn1IfNumberIs1()
    {
        $this->assertEquals("1", $this->fizzBuzz->run(1, 1));
    }

    /**
     * @test
     */
    public function shouldReturn2IfNumberIs2()
    {
        $this->assertEquals("2", $this->fizzBuzz->run(2, 2));
    }

    /**
     * @test
     */
    public function shouldReturnFizzIfNumberIs3()
    {
        $this->assertEquals("Fizz", $this->fizzBuzz->run(3, 3));
    }

    /**
     * @test
     */
    public function shouldReturnFizzIfNumberIs6()
    {
        $this->assertEquals("Fizz", $this->fizzBuzz->run(6, 6));
    }

    /**
     * @test
     */
    public function shouldReturnBuzzIfNumberIs5()
    {
        $this->assertEquals("Buzz", $this->fizzBuzz->run(5, 5));
    }

    /**
     * @test
     */
    public function shouldReturnBuzzIfNumberIs10()
    {
        $this->assertEquals("Buzz", $this->fizzBuzz->run(10, 10));
    }

    /**
     * @test
     */
    public function shouldReturnFizzBuzzIfNumberIs15()
    {
        $this->assertEquals("FizzBuzz", $this->fizzBuzz->run(15, 15));
    }

    /**
     * @test
     */
    public function shouldReturnFizzBuzzIfNumberIs30()
    {
        $this->assertEquals("FizzBuzz", $this->fizzBuzz->run(30, 30));
    }

    /**
     * @test
     */
    public function shouldReturn12IfNumbersAre1To2()
    {
        $this->assertEquals("12", $this->fizzBuzz->run(1, 2));
    }

    /**
     * @test
     */
    public function shouldReturn12FizzIfNumbersAre1To3()
    {
        $this->assertEquals("12Fizz", $this->fizzBuzz->run(1, 3));
    }

    /**
     * @test
     */
    public function shouldReturn12Fizz4BuzzIfNumbersAre1To5()
    {
        $this->assertEquals("12Fizz4Buzz", $this->fizzBuzz->run(1, 5));
    }

    /**
     * @test
     */
    public function shouldReturn12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzzIfNumbersAre1To15()
    {
        $this->assertEquals("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", $this->fizzBuzz->run(1, 15));
    }
}
